const callback2 = require("../callback2.cjs")

const ID = "mcu453ed"

callback2(ID, (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log("Details for '" + ID + "' Id:");
    console.log(data);
  }
});