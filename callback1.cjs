const boards = require('./boards.json');

function callback1(boardID, callback) {

  if (boardID != undefined && typeof boardID == 'string' && callback != undefined && typeof callback == 'function') {

    setTimeout(() => {

      const board = boards.find(board => {
        return board.id === boardID
      });

      if (!board) {
        callback(boardID + ": unregistered ID");
      } else {
        callback(null, board);
      }
    }, 2 * 1000);

  } else {
    console.log('ERROR: data invalid');
    return;
  }
}



module.exports = callback1;