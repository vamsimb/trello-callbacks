const lists = require('./lists.json');

function callback2(boardID, callback) {

  if (boardID != undefined && typeof boardID == 'string' && callback != undefined && typeof callback == 'function') {

    setTimeout(() => {

      if (!lists[boardID]) {
        callback(boardID + ": unregistered ID");
      } else {
        callback(null, lists[boardID]);
      }
    }, 2 * 1000);

  } else {
    console.log('ERROR: data invalid');
    return;
  }
}



module.exports = callback2;