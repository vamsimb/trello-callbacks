const thanosBoardsdata = require("./callback1.cjs")
const thanosBoardList = require("./callback2.cjs")
const getMindListCards = require("./callback3.cjs")

function callback4(thanosID = 'mcu453ed', listName = 'Mind') {

  setTimeout(() => {

    thanosBoardsdata(thanosID, (err, boardData) => {

      if (err) {
        console.error(err)
        return
      } else {
        console.log(boardData.name, "Boardsdetails: ")
        console.log(boardData)

        thanosBoardList(thanosID, (err, listData) => {

          if (err) {
            console.error(err)
          } else {
            console.log("Thanos Board List: ")
            console.log(listData)

            let mindStoneId = listData.find(stone => {
              return (stone.name == listName)
            })

            getMindListCards(mindStoneId.id, (err, stonedata) => {

              if (err) {
                console.error(err)
              } else {
                console.log("Mind List Cards: ")
                console.log(stonedata)
              }
            })
          }
        });
      }
    })
  }, 2 * 1000)
}

module.exports = callback4;