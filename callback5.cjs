const thanosBoardsdata = require("./callback1.cjs")
const thanosBoardList = require("./callback2.cjs")
const getMindAndSpaceListCards = require("./callback3.cjs")

function callback5(thanosID = 'mcu453ed') {

  setTimeout(() => {

    thanosBoardsdata(thanosID, (err, boardData) => {

      if (err) {
        console.error(err)
        return
      } else {
        console.log(boardData.name, "Boardsdetails: ")
        console.log(boardData)

        thanosBoardList(thanosID, (err, Listdata) => {

          if (err) {
            console.error(err)
            return
          } else {
            console.log("Thanos Board List: ")
            console.log(Listdata)

            requiredStones = ['Mind', 'Space']

            for (let stone of Listdata) {

              if (requiredStones.includes(stone.name)) {

                getMindAndSpaceListCards(stone.id, (err, stonedata) => {

                  if (err) {
                    console.error(err)
                    return
                  } else {
                    console.log(stone.name, "List Cards: ")
                    console.log(stonedata)
                  }
                })
              }
            }
          }
        });
      }
    })
  }, 2 * 1000)
}

module.exports = callback5;