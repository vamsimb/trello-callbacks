const cards = require('./cards.json');

function callback3(listID, callback) {

  if (listID != undefined && typeof listID == 'string' && callback != undefined && typeof callback == 'function') {

    setTimeout(() => {

      if (!cards[listID]) {
        callback(listID + ": unregistered ID");
      } else {
        callback(null, cards[listID]);
      }
      
    }, 2 * 1000);

  } else {
    callback('ERROR: data invalid');
    return;
  }
}


module.exports = callback3;